# Crating an online store website using Godaddy 


##  -  - Create an account with Godaddy.
##  - Go to ```Build```
##  - Click on ```Start for free```

![](./1.png)

##  - Choose the Online store from the options.

![](./2.png)

##  - You can design your website or choose from the available themes.

![](./3.png)

##  - In order to add product to our webstite (meaning by that take payment or creating an inventory) then we need to click on ``` website builder```

![](./4.png)

##  - We will choose the Stock your online store and then we will click on ```Add products```

![](./5.png)

##  - Gice a description to the product, inventory and options and also the shipping method.

![](./6.png)

##  - Let's add a payment method to our website by choosing ```Enable credit card payment``` and then click on ```Take payment```

![](./7.png)

##  - here we can choose from the payment method however Paypall is the most afordable and easy to use. 

![](./8.png)


##  - Lastly, Click on publish to have your website running on the cloud and make sure you have a domain name for it. 








